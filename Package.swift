// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "ShoppingList",
    products: [
        .executable(
            name: "ShoppingList",
            targets: ["ShoppingList"]
        ),
    ],
    dependencies: [
        .package(url: "https://github.com/thinkaboutiter/MongoDB-StORM.git", Package.Dependency.Requirement._revisionItem("36b9eb7b94d71a5676d8dfe2aaf37306575c3576")),
        .package(url: "https://github.com/PerfectlySoft/Perfect-HTTPServer.git", from: "3.0.0"),
        .package(url: "https://github.com/thinkaboutiter/SimpleLogger.git", from: "2.0.0")
    ],
    targets: [
        .target(
            name: "ShoppingList",
            dependencies: [
                "PerfectHTTPServer",
                "MongoDBStORM",
                "SimpleLogger"
            ],
            path: "Sources"
        ),
        .testTarget(
            name: "ShoppingListTests",
            dependencies: [
                "ShoppingList",
            ],
            path: "Tests"
        )
    ]
)
